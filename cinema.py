#This program is a Cinema with 4 movies available to watch, including the age limit and number of seats available.

#you will be asked what movie you want to watch, and then be asked how old you are. if the movie does not exist, you will recieve: "sorry, We don't have that movie..."

#if you meet the age requirement, you will be told to enjoy the film. If you don't meet the age requirement, "GTFO! You're too young to see this movie kid."

#The while loop will allow you to purchase tickets for the film until the number of seats are taken up. "Sorry, we are sold  out!"


films = {
    "The Grinch": [3,5],
    "Replica": [18,5],
    "Aquaman": [15,5],
    "Bumblebee":[12,5]
    }

while True:

    choice = input("What movie do you want to watch?: ").strip().title()

    if choice in films:
        age = int(input("How old are you?: ").strip())

        #Check user age

        if age >= films[choice][0]:
            #check enough seats

            num_seats = films[choice][1]

            if num_seats > 0:
                print("Enjoy the film!")
                films[choice][1] = films[choice][1] -1
            else:
                print("Sorry, we are sold  out!)
        else:
                print("GTFO! You're too young to see this movie kid.")
                
    else:
        print("sorry, We don't have that movie...")